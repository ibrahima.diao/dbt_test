from flask import Flask,request
import os
import subprocess
import logging

app = Flask(__name__)

@app.route('/run_models',methods=["Post"])
def run_models():
    logging.info("helloworld: received a request")
    try:
        model=request.form.get('models')
        cmd = ['dbt', 'run', '--target', 'dev', '--profiles-dir', '.', '--select', str(model)]
        result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        result.check_returncode()
        print(result.stdout.decode())
    except subprocess.CalledProcessError as e:
        logging.error(f'Execution failed with error {e.returncode}\n{e.stderr.decode()}')
        return 'An error occurred', 500
    return 'Success', 200

# method post to run test models 
@app.route('/test_models',methods=["Post"])
def test_models():
    logging.info("helloworld: received a request")
    try:
        model=request.form.get('models')
        cmd = ['dbt','test', '--target', 'dev', '--profiles-dir', '.', '--select', str(model)]

        result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        result.check_returncode()
        logging.info(result.stdout.decode())
    except subprocess.CalledProcessError as e:
        logging.info(result.stdout.decode())
        logging.error(f'Execution failed with error {e.returncode}\n{e.stderr.decode()}')
        return 'Test failed', 500
    return 'Test Success', 200



if __name__ == '__main__':
    
    logging.basicConfig(level=logging.INFO)
    logging.info("helloworld: starting server...")
    port = os.getenv("PORT")
    if port is None:
        port = "8080"
    logging.info(f"helloworld: listening on {port}")
    app.run(host='0.0.0.0', port="8080")
