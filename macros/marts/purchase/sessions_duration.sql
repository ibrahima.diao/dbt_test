-- macros/my_macro.sql
{% macro sessions_duration(table_name, time_column) %}
WITH data_events_extended AS (
  SELECT * FROM {{ ref(table_name) }}
),

sessions_duration AS (
  SELECT
    user_session,
    MIN({{ time_column }}) AS amin,
    MAX({{ time_column }}) AS amax
  FROM data_events_extended
  GROUP BY user_session
)

SELECT * FROM sessions_duration
{% endmacro %}
