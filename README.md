# Projet de Pipeline CI/CD avec GitLab, dbt et Google Cloud Run

Ce projet illustre comment mettre en place un pipeline CI/CD pour automatiser les tâches d'analyse de données et de transformation à l'aide de GitLab, dbt et Google Cloud Run.

## Présentation

Le pipeline proposé utilise GitLab pour gérer les versions et les mises à jour du code, dbt pour les transformations de données, et Google Cloud Run pour déployer et exécuter les tâches automatisées.

### dbt

dbt (Data Build Tool) est un outil open-source pour transformer et analyser les données dans les entrepôts de données. dbt est spécifiquement conçu pour travailler avec SQL et permet de créer des modèles de données réutilisables, des tests pour valider la qualité des données, et des snapshots pour capturer les modifications de données au fil du temps.

### Google Cloud Run

Google Cloud Run est un service d'hébergement sans serveur qui exécute des conteneurs Docker. Il permet de déployer et d'exécuter des applications et des tâches automatisées sans avoir à gérer l'infrastructure sous-jacente.

## Configuration du projet

1. Créez un nouveau projet GitLab et ajoutez votre code source, y compris vos modèles dbt, fichiers de configuration, tests, etc.
2. Configurez GitLab CI/CD en ajoutant un fichier `.gitlab-ci.yml` à la racine du projet, définissant les étapes de build, test et déploiement.
3. Créez un compte de service Google Cloud avec les autorisations nécessaires pour déployer et gérer les services Cloud Run.
4. Ajoutez les informations d'identification du compte de service Google Cloud et les variables d'environnement nécessaires à GitLab CI/CD.
5. Configurez Google Cloud Run pour exécuter les tâches automatisées définies dans votre conteneur Docker.
6. Configurez votre fichier `profiles.yml` pour définir les environnements de développement et de production pour dbt en utilisant BigQuery comme entrepôt de données

## Utilisation

Une fois le pipeline CI/CD configuré, il se déclenchera automatiquement à chaque modification du code source. Les transformations de données dbt seront exécutées sur Google Cloud Run, et les résultats seront disponibles dans votre entrepôt de données BigQuery.

Les développeurs peuvent continuer à travailler sur le code source et à soumettre des modifications à GitLab, qui se chargeront de gérer le pipeline CI/CD et de déployer les mises à jour sur Google Cloud Run

## Fichiers importants

- `cloudbuild.yaml` : Contient les instructions pour construire, pousser et déployer votre application sur Google Cloud Run.
- `.gitlab-ci.yml` : Contient les instructions pour le pipeline CI/CD GitLab.
- `profiles.yml` : Contient les configurations des environnements de développement et de production.
