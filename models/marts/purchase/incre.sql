{{ config(
    materialized='incremental'
)}}

select *
FROM {{ ref('data_events_extended') }}
where event_time > '2019-10-13'