{{ config(materialized='incremental') }}

WITH data_events_extended AS (
  SELECT * FROM {{ ref('data_events_extended') }}
),

sessions_duration AS (
  SELECT
    user_session,
    MIN(event_time) AS amin,
    MAX(event_time) AS amax,
  FROM data_events_extended
  GROUP BY user_session
)

SELECT * FROM sessions_duration
