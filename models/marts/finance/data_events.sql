
{{ config(materialized='table') }}

WITH data_sessions AS (
  SELECT *
  FROM  `dbt.event` 
  WHERE user_session IS NOT NULL
),

events_per_session AS (
  SELECT
    user_session,
    product_id,
    MAX(CASE WHEN event_type = 'purchase' THEN 1 ELSE 0 END) AS purchased,
    SUM(CASE WHEN event_type = 'view' THEN 1 ELSE 0 END) AS num_views_product
  FROM data_sessions
  GROUP BY user_session, product_id
),

views_per_session AS (
  SELECT
    user_session,
    SUM(CASE WHEN event_type = 'view' THEN 1 ELSE 0 END) AS num_views_session
  FROM data_sessions
  GROUP BY user_session
),

data_events AS (
  SELECT ds.*, eps.purchased, eps.num_views_product, vps.num_views_session
  FROM data_sessions ds
  JOIN events_per_session eps ON ds.user_session = eps.user_session AND ds.product_id = eps.product_id
  JOIN views_per_session vps ON ds.user_session = vps.user_session
),
data_events_converted AS (
  SELECT *,
    CAST(event_time AS TIMESTAMP) AS event_time_ts
  FROM data_events
),

data_events_extended AS (
  SELECT
    *,
    EXTRACT(HOUR FROM event_time_ts) AS hour,
    EXTRACT(MINUTE FROM event_time_ts) AS minute,
   
  FROM data_events_converted
)

SELECT * FROM data_events_extended
