# On part d'une image Python 3.8
#FROM python:3.8 as builder

# Créer le répertoire de travail
#WORKDIR /app

# Copier le fichier Python
#COPY invoke.py ./

# Installation de Flask
#RUN pip install flask

# Base image
#FROM ghcr.io/dbt-labs/dbt-bigquery:1.2.latest
FROM ghcr.io/dbt-labs/dbt-bigquery:1.5.1

# Passer en root pour les commandes nécessaires
#USER root

# Créer le répertoire de travail
WORKDIR /dbt

# Copier le fichier Python du builder
#COPY --from=builder /app/invoke.py ./

# Copier le script shell
#COPY script.sh ./

# Copier le reste des fichiers
COPY . ./
RUN apt-get update && apt-get install -y python3-pip && pip3 install flask

# Rendre le script Python exécutable
RUN chmod +x invoke.py

# Exécuter le script Python
ENTRYPOINT ["python", "./invoke.py"]
